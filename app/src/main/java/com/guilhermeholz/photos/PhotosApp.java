package com.guilhermeholz.photos;

import android.app.Application;
import android.content.Context;

import com.guilhermeholz.photos.di.components.AppComponent;
import com.guilhermeholz.photos.di.components.DaggerAppComponent;
import com.guilhermeholz.photos.di.modules.AppModule;
import com.guilhermeholz.photos.di.modules.NetworkModule;
import com.guilhermeholz.photos.di.modules.RealmModule;

/**
 * Created by gholz on 6/6/16.
 */
public class PhotosApp extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {

        super.onCreate();

        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .realmModule(new RealmModule(this))
                .build();
    }

    public static PhotosApp getApplication(Context context) {
        return (PhotosApp) context.getApplicationContext();
    }

    public AppComponent getComponent() {
        return component;
    }
}
