package com.guilhermeholz.photos.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.text.TextUtils;
import android.util.Log;

import com.guilhermeholz.photos.PhotosApp;
import com.guilhermeholz.photos.domain.PhotosRepository;
import com.guilhermeholz.photos.model.Photo;
import com.guilhermeholz.photos.view.PhotoGridView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by gholz on 6/6/16.
 */
public class PhotoGridViewModel extends BaseObservable {

    private static final String LOG_TAG = PhotoGridViewModel.class.getSimpleName();

    public ObservableBoolean displayProgress = new ObservableBoolean(true);
    public ObservableBoolean displayContent = new ObservableBoolean();
    public ObservableBoolean displayError = new ObservableBoolean();
    public ObservableBoolean displayEmpty = new ObservableBoolean();

    private ObservableBoolean[] displayProperties = {
            displayProgress,
            displayContent,
            displayError,
            displayEmpty
    };

    private PhotoGridView view;
    private List<Photo> allPhotos;

    @Inject
    protected PhotosRepository repository;

    public PhotoGridViewModel(Context context, PhotoGridView view) {
        this.view = view;
        allPhotos = new ArrayList<>();
        PhotosApp.getApplication(context)
                .getComponent().inject(this);
    }

    public void loadPhotos() {
        repository.getPhotos()
                .subscribe(photos -> {
                    if (!photos.isEmpty()) {
                        toggle(displayContent);
                        view.displayPhotos(photos);
                        allPhotos.clear();
                        allPhotos.addAll(photos);
                    } else {
                        toggle(displayEmpty);
                    }
                }, throwable -> {
                    toggle(displayError);
                    Log.e(LOG_TAG, "Error loading photos", throwable);
                });
    }

    private void toggle(ObservableBoolean itemToDisplay) {
        for (ObservableBoolean item : displayProperties) {
            item.set(item.equals(itemToDisplay));
        }
    }

    public void filter(String query) {
        if (TextUtils.isEmpty(query)) {
            view.displayPhotos(allPhotos);
        } else {
            Observable.from(allPhotos)
                    .filter(photo -> photo.title.contains(query))
                    .toList()
                    .subscribe(view::displayPhotos);
        }
    }
}
