package com.guilhermeholz.photos.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.view.View;

import com.guilhermeholz.photos.model.Photo;
import com.guilhermeholz.photos.view.PhotoGridView;

/**
 * Created by gholz on 6/7/16.
 */
public class PhotoItemViewModel extends BaseObservable {

    public ObservableField<String> url = new ObservableField<>();
    public ObservableField<String> description = new ObservableField<>();

    private PhotoGridView view;
    private long photoId;

    public PhotoItemViewModel(PhotoGridView view) {
        this.view = view;
    }

    public void setPhoto(Photo photo) {

        /* Not really necessary with the sample data, just wanted to show
        a simple use case of the viewModel data handling */
        if (photo.thumbnailUrl != null) {
            url.set(photo.thumbnailUrl);
        } else {
            url.set(photo.url);
        }

        description.set(photo.title);
        photoId = photo.id;
    }

    public void onClickItem(View photoView) {
        view.displayPhotoDetail(photoId, photoView);
    }
}
