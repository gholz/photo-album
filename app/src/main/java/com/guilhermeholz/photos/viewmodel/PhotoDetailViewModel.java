package com.guilhermeholz.photos.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.util.Log;

import com.guilhermeholz.photos.PhotosApp;
import com.guilhermeholz.photos.domain.PhotosRepository;

import javax.inject.Inject;

/**
 * Created by gholz on 6/6/16.
 */
public class PhotoDetailViewModel extends BaseObservable {

    private static final String LOG_TAG = PhotoDetailViewModel.class.getSimpleName();

    public ObservableField<String> title = new ObservableField<>();
    public ObservableField<String> url = new ObservableField<>();

    @Inject
    protected PhotosRepository repository;

    public PhotoDetailViewModel(Context context, long id) {

        PhotosApp.getApplication(context)
                .getComponent().inject(this);

        loadPhoto(id);
    }

    private void loadPhoto(long id) {
        repository.getPhoto(id)
                .subscribe(photo -> {
                            title.set(photo.title);
                            url.set(photo.url);
                        },
                        throwable -> {
                            Log.e(LOG_TAG, "Error loading photo " + id, throwable);
                        });
    }
}
