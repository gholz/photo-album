package com.guilhermeholz.photos.di.modules;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.guilhermeholz.photos.PhotosApp;
import com.guilhermeholz.photos.domain.PhotosRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by gholz on 6/2/16.
 */
@Module
public class AppModule {

    private PhotosApp application;

    public AppModule(PhotosApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public PhotosRepository provideRepository() {
        return new PhotosRepository(application);
    }

    @Provides
    @Singleton
    public SharedPreferences providePreferences() {
        return PreferenceManager.getDefaultSharedPreferences(application);
    }
}
