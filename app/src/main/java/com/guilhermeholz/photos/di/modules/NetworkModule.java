package com.guilhermeholz.photos.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guilhermeholz.photos.domain.PhotosApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gholz on 6/2/16.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    public PhotosApi provideApi(GsonConverterFactory converterFactory,
                                RxJavaCallAdapterFactory callAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com")
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .build()
                .create(PhotosApi.class);
    }

    @Provides
    @Singleton
    public RxJavaCallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Provides
    @Singleton
    public GsonConverterFactory provideGsonConverter(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().create();
    }
}
