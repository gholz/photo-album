package com.guilhermeholz.photos.di.modules;

import android.content.Context;

import com.guilhermeholz.photos.domain.PhotosRealmDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by gholz on 6/7/16.
 */
@Module
public class RealmModule {

    private Context context;

    public RealmModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public RealmConfiguration provideConfiguration() {
        return new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
                .build();
    }

    @Provides
    @Singleton
    public Realm provideRealm(RealmConfiguration configuration) {
        return Realm.getInstance(configuration);
    }

    @Provides
    @Singleton
    public PhotosRealmDataSource providesDataSource(Realm realm) {
        return new PhotosRealmDataSource(realm);
    }
}
