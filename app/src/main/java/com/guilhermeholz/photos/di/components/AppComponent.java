package com.guilhermeholz.photos.di.components;

import com.guilhermeholz.photos.di.modules.RealmModule;
import com.guilhermeholz.photos.domain.PhotosRepository;
import com.guilhermeholz.photos.viewmodel.PhotoDetailViewModel;
import com.guilhermeholz.photos.viewmodel.PhotoGridViewModel;
import com.guilhermeholz.photos.di.modules.AppModule;
import com.guilhermeholz.photos.di.modules.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by gholz on 6/2/16.
 */
@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, RealmModule.class})
public interface AppComponent {
    void inject(PhotoGridViewModel viewModel);
    void inject(PhotoDetailViewModel viewModel);
    void inject(PhotosRepository repository);
}
