package com.guilhermeholz.photos.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by gholz on 6/6/16.
 */
public class Photo extends RealmObject {
    @PrimaryKey
    public long id;
    public String title;
    public String url;
    public String thumbnailUrl;
}
