package com.guilhermeholz.photos.ui.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.Window;

import com.guilhermeholz.photos.R;
import com.guilhermeholz.photos.databinding.PhotoGridBinding;
import com.guilhermeholz.photos.model.Photo;
import com.guilhermeholz.photos.ui.adapters.PhotoGridAdapter;
import com.guilhermeholz.photos.view.PhotoGridView;
import com.guilhermeholz.photos.viewmodel.PhotoGridViewModel;

import java.util.List;

public class PhotoGridActivity extends AppCompatActivity implements PhotoGridView {

    private PhotoGridAdapter adapter;
    private PhotoGridViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requestWindowFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }

        adapter = new PhotoGridAdapter(this, this);
        viewModel = new PhotoGridViewModel(this, this);

        PhotoGridBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_photo_grid);
        binding.photosGrid.setLayoutManager(new GridLayoutManager(this, 2));
        binding.photosGrid.setAdapter(adapter);
        binding.searchBar.setIconifiedByDefault(false);
        binding.searchBar.setOnQueryTextListener(queryListener);
        binding.setViewModel(viewModel);

        viewModel.loadPhotos();
    }

    @Override
    public void displayPhotos(List<Photo> photos) {
        adapter.setPhotos(photos);
    }

    @Override
    public void displayPhotoDetail(long id, View view) {

        Intent intent = new Intent(this, PhotoDetailActivity.class);
        intent.putExtra(PhotoDetailActivity.EXTRA_PHOTO_ID, id);

        ActivityOptionsCompat options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this, view, "imageDetail");
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }

    private SearchView.OnQueryTextListener queryListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            viewModel.filter(newText);
            return true;
        }
    };
}
