package com.guilhermeholz.photos.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guilhermeholz.photos.R;
import com.guilhermeholz.photos.databinding.PhotoItemBinding;
import com.guilhermeholz.photos.model.Photo;
import com.guilhermeholz.photos.view.PhotoGridView;
import com.guilhermeholz.photos.viewmodel.PhotoItemViewModel;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.RecyclerView.*;

/**
 * Created by gholz on 6/6/16.
 */
public class PhotoGridAdapter extends RecyclerView.Adapter<PhotoGridAdapter.PhotoItemViewHolder> {

    private LayoutInflater inflater;
    private List<Photo> photos;
    private PhotoGridView view;

    public PhotoGridAdapter(Context context, PhotoGridView view) {
        inflater = LayoutInflater.from(context);
        photos = new ArrayList<>();
        this.view = view;
    }

    @Override
    public PhotoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        PhotoItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.item_photo, parent, false);
        binding.setViewModel(new PhotoItemViewModel(view));

        return new PhotoItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PhotoItemViewHolder holder, int position) {
        holder.bind(photos.get(position));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(List<Photo> photos) {
        this.photos.clear();
        this.photos.addAll(photos);
        notifyDataSetChanged();
    }

    public static class PhotoItemViewHolder extends ViewHolder {

        private PhotoItemBinding binding;

        public PhotoItemViewHolder(PhotoItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Photo photo) {
            binding.getViewModel().setPhoto(photo);
        }
    }
}
