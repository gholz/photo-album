package com.guilhermeholz.photos.ui.util;

import android.graphics.Point;
import android.support.annotation.NonNull;
import android.view.Display;

/**
 * Created by gholz on 6/9/16.
 */
public class MetricsUtils {

    public static int getDisplayWidth(Display display) {
        return getScreenSize(display).x;
    }

    public static int getDisplayHeight(Display display) {
        return getScreenSize(display).y;
    }

    @NonNull
    private static Point getScreenSize(Display display) {
        Point size = new Point();
        display.getSize(size);
        return size;
    }
}
