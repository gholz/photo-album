package com.guilhermeholz.photos.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.guilhermeholz.photos.R;
import com.guilhermeholz.photos.databinding.PhotoDetailBinding;
import com.guilhermeholz.photos.viewmodel.PhotoDetailViewModel;

/**
 * Created by gholz on 6/6/16.
 */
public class PhotoDetailActivity extends AppCompatActivity {

    public static final String EXTRA_PHOTO_ID = "photo_id";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            requestWindowFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }

        long id = getIntent().getExtras().getLong(EXTRA_PHOTO_ID);

        PhotoDetailViewModel viewModel = new PhotoDetailViewModel(this, id);

        PhotoDetailBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_photo_detail);
        binding.setViewModel(viewModel);
    }
}
