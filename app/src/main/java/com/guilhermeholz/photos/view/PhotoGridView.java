package com.guilhermeholz.photos.view;

import android.view.View;

import com.guilhermeholz.photos.model.Photo;

import java.util.List;

/**
 * Created by gholz on 6/7/16.
 */
public interface PhotoGridView {
    void displayPhotos(List<Photo> photos);
    void displayPhotoDetail(long id, View view);
}
