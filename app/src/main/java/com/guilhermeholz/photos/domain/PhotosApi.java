package com.guilhermeholz.photos.domain;

import com.guilhermeholz.photos.model.Photo;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by gholz on 6/6/16.
 */
public interface PhotosApi {
    @GET("/photos?albumId=2")
    Observable<List<Photo>> getPhotos();
}
