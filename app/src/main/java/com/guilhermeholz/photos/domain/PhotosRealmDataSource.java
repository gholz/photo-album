package com.guilhermeholz.photos.domain;

import com.guilhermeholz.photos.model.Photo;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;

/**
 * Created by gholz on 6/7/16.
 */
public class PhotosRealmDataSource {

    private Realm realm;

    public PhotosRealmDataSource(Realm realm) {
        this.realm = realm;
    }

    public void addPhotos(List<Photo> photos) {
        realm.executeTransaction(r -> r.copyToRealmOrUpdate(photos));
    }

    public Observable<List<Photo>> getPhotos() {
        return realm.where(Photo.class).findAllAsync().asObservable()
                .map(this::toArrayList);
    }

    private List<Photo> toArrayList(RealmResults<Photo> results) {

        ArrayList<Photo> photos = new ArrayList<>();
        for (Photo photo : results) {
            photos.add(photo);
        }

        return photos;
    }

    public long getCount() {
        return realm.where(Photo.class).count();
    }

    public Observable<Photo> getPhoto(long id) {
        return realm.where(Photo.class)
                .equalTo("id", id)
                .findFirstAsync()
                .<Photo>asObservable()
                .filter(photo -> photo.isLoaded())
                .first();
    }
}
