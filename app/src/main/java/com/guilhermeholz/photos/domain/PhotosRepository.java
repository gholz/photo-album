package com.guilhermeholz.photos.domain;

import android.content.Context;
import android.util.Log;

import com.guilhermeholz.photos.PhotosApp;
import com.guilhermeholz.photos.model.Photo;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by gholz on 6/6/16.
 */
public class PhotosRepository {

    private static final String LOG_TAG = PhotosRepository.class.getSimpleName();

    @Inject
    protected PhotosApi api;

    @Inject
    protected PhotosRealmDataSource dataSource;

    public PhotosRepository(Context context) {
        PhotosApp.getApplication(context).getComponent().inject(this);
    }

    public Observable<List<Photo>> getPhotos() {

        if (dataSource.getCount() > 0) {

            api.getPhotos().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(dataSource::addPhotos, throwable -> {
                        Log.e(LOG_TAG, "Loading photos error", throwable);
                    });

            return dataSource.getPhotos();
        } else {
            return api.getPhotos()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(this::persist);
        }
    }

    private Observable<List<Photo>> persist(List<Photo> photos) {
        dataSource.addPhotos(photos);
        return dataSource.getPhotos();
    }

    public Observable<Photo> getPhoto(long id) {
        return dataSource.getPhoto(id);
    }
}
